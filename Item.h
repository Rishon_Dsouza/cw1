/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <vector>
/*
 *@brief declaration of base class with shared attributes
 */
class Item {
   protected
       :  // any vars need to be accessed by child classes so are made protected
    int id, quantity;
    std::string title;
    float price;
    // std::string name;

   public:  // constructor and methods are public
    Item(int id, std::string title, float price,
         int quantity);  // base constructor declared
    Item();
    int getid();  // getters and setters declared
    void setid(int id);
    std::string gettitle();
    void settitle(std::string title);
    int getquantity();
    void setquantity(int quantity);
    float getprice();
    void setprice(float price);
    virtual std::vector<std::string> list() = 0;
};
#endif