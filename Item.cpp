/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#include "Item.h"

#include <iostream>
/*
 *@brief base class that has shared attributes
 *@params id a unique identifier for the item
 *@params title is the name of the item
 *@params price is the cost of 1 unit of the item
 *@params quantity is the number of items
 */
Item::Item(int id, std::string title, float price, int quantity)
    : id(id), title(title), price(price), quantity(quantity) {}

int Item::getid() { return this->id; }
void Item::setid(int id) { this->id = id; }
std::string Item::gettitle() { return this->title; }
void Item::settitle(std::string title) { this->title = title; }
int Item::getquantity() { return this->quantity; }
void Item::setquantity(int quantity) { this->quantity = quantity; }
float Item::getprice() { return this->price; }
void Item::setprice(float price) { this->price = price; }