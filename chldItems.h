/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#ifndef CHLDITEMS_H
#define CHLDITEMS_H
#include <string>
#include <vector>

#include "Item.h"
/*
 *@brief CD is child subclass of item class
 */
class CD : public Item {
   private:
    std::string album;  // delclare all private vars used in class
    int year;

   public:
    CD(int id, std::string title, std::string album, int year, float price,
       int quantity);  // defualt constructors
    CD();
    std::vector<std::string> list();  // a listing function to list all CDs
    std::string getalbum();
    void setalbum(std::string album);
    int getyear();
    void setyear(int year);
};
/*
 *@brief DVD is child subclass of item class
 */
class DVD : public Item {
   private:
    int year;

   public:
    DVD(int id, std::string title, int year, float price, int quantity);
    DVD();
    int getyear();
    void setyear(int year);
    std::vector<std::string> list();
};
/*
 *@brief Boook is child subclass of item class
 */
class Book : public Item {
   private:
    std::string author, isbn;
    int year;

   public:
    Book(int id, std::string title, std::string author, std::string isbn,
         int price, int quantity);
    Book();
    std::string getauthor();
    void setauthor(std::string author);
    std::string getisbn();
    void setisbn(std::string isbn);
    std::vector<std::string> list();
};
/*
 *@brief Magezine is child subclass of item class
 */
class Magezine : public Item {
   private:
    int issue, year;
    std::string isbn;

   public:
    Magezine(int id, std::string title, int issue, int year, std::string isbn,
             int price, int quantity);
    Magezine();
    int getissue();
    void setissue(int issue);
    std::string getisbn();
    void setisbn(std::string isbn);
    int getyear();
    void setyear(int year);
    std::vector<std::string> list();
};

#endif
