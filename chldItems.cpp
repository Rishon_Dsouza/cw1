/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#include "chldItems.h"

#include <iostream>
#include <vector>
/*
 *@breif subclass of Item
 *@params id, title params from Item class
 *@params album is name of album that the CD's title was part of
 */
CD::CD(int id, std::string title, std::string album, int year, float price,
       int quantity)
    : Item(id, title, price, quantity), album(album) {}
std::vector<std::string> CD::list() {
    return {std::to_string(getid()),
            gettitle(),
            getalbum(),
            std::to_string(getyear()),
            std::to_string(getprice()),
            std::to_string(getprice())};
}

std::string CD::getalbum() { return this->album; };
void CD::setalbum(std::string album) { this->album = album; };
int CD::getyear() { return this->year; };
void CD::setyear(int year) { this->year = year; };
/*
 *@breif subclass of Item
 *@params id, title params from Item class
 */
DVD::DVD(int id, std::string title, int year, float price, int quantity)
    : Item(id, title, price, quantity), year(year) {}
int DVD::getyear() { return this->year; };
void DVD::setyear(int year) { this->year = year; };
std::vector<std::string> DVD::list() {
    return {
        std::to_string(getid()),       gettitle(),
        std::to_string(getyear()),     std::to_string(getprice()),
        std::to_string(getquantity()),
    };
}
/*
 *@breif subclass of Item
 *@params id, title params from Item class
 *@params author is the writer of the book
 *@params isbn is another way to identify the book
 */
Book::Book(int id, std::string title, std::string author, std::string isbn,
           int price, int quantity)
    : Item(id, title, price, quantity), author(author), isbn(isbn){};
std::string Book::getauthor() { return this->author; };
void Book::setauthor(std::string author) { this->author = author; };
std::string Book::getisbn() { return this->isbn; };
void Book::setisbn(std::string isbn) { this->isbn = isbn; };
std::vector<std::string> Book::list() {
    return {std::to_string(getid()),
            gettitle(),
            getauthor(),
            getisbn(),
            std::to_string(getprice()),
            std::to_string(getquantity())};
};
/*
 *@breif subclass of Item
 *@params id, title params from Item class
 *@params isbn to identify the magezine specifically
 *@params issue the issue of the magezine
 */
Magezine::Magezine(int id, std::string title, int issue, int year,
                   std::string isbn, int price, int quantity)
    : Item(id, title, price, quantity), issue(issue), year(year), isbn(isbn){};
int Magezine::getissue() { return this->issue; };
void Magezine::setissue(int issue) { this->issue = issue; };
int Magezine::getyear() { return this->year; };
std::string Magezine::getisbn() { return this->isbn; };
std::vector<std::string> Magezine::list() {
    return {
        std::to_string(getid()),      gettitle(), std::to_string(getissue()),
        std::to_string(getyear()),    getisbn(),  std::to_string(getprice()),
        std::to_string(getquantity())};
};
